retention_query_bq = """
SELECT 
  sessions_date, 
  retention_date, 
  users_number, 
  max(users_number) OVER(PARTITION BY category) AS all_user_number, 
  category 
FROM ( 
  SELECT 
    sessions_date, 
    retention_date, 
    category, 
    SUM(CASE 
        WHEN user_id IS NOT NULL THEN 1 
        ELSE 0 END) AS users_number 
  FROM ( 
    SELECT 
      user_id, 
      sessions_date, 
      DATEDIFF(sessions_date, first_date) AS retention_date, 
      category 
    FROM ( 
      SELECT 
        device.advertising_id  as user_id, 
        event_date AS sessions_date, 
        MIN(event_date) OVER(PARTITION BY user_id) AS first_date, 
        %category_key% AS category 
      FROM 
        TABLE_DATE_RANGE([the-cooking-game.analytics_153765032.events_], 
          TIMESTAMP('%start_dt_key%'), 
                             TIMESTAMP('%end_dt_key%')) ) 
    GROUP BY
      1,
      2,
      3,
      4)
  GROUP BY
    1,
    2,
    3)
"""



retention_query_bq_af = """
SELECT 
  sessions_date, 
  retention_date, 
  users_number, 
  max(users_number) OVER(PARTITION BY category) AS all_user_number, 
  category 
FROM ( 
  SELECT 
    sessions_date, 
    retention_date, 
    category, 
    SUM(CASE 
        WHEN user_id IS NOT NULL THEN 1 
        ELSE 0 END) AS users_number 
  FROM ( 
        SELECT 
          user_id, 
          sessions_date, 
          DATEDIFF(sessions_date, first_date) AS retention_date, 
          category 
        FROM ( 
          SELECT
              bq.user_id as user_id,
              bq.sessions_date as sessions_date,
              bq.first_date as first_date,
              apps.category as category
          FROM
          (SELECT 
            device.advertising_id as user_id, 
            event_date AS sessions_date, 
            MIN(event_date) OVER(PARTITION BY user_id) AS first_date
          FROM 
            TABLE_DATE_RANGE([the-cooking-game.analytics_153765032.events_], 
              TIMESTAMP('%start_dt_key%'), 
                             TIMESTAMP('%end_dt_key%')) )  as bq
        left join 
        (
        SELECT
          IFNULL(IDFA, Advertising_ID) as user_id,
          %category_key% as category,
          count(*)
        FROM
          TABLE_DATE_RANGE([the-cooking-game.appsflyer_IOS.in_app_events_],
            TIMESTAMP('%start_dt_key%'), 
                             TIMESTAMP('%end_dt_key%')),
          TABLE_DATE_RANGE([the-cooking-game.appsflyer_android.in_app_events_],
            TIMESTAMP('%start_dt_key%'), 
                             TIMESTAMP('%end_dt_key%'))
        group by 
            user_id,
            category
        ) as apps
        on bq.user_id = apps.user_id)
    GROUP BY
      1,
      2,
      3,
      4 )
  GROUP BY
    1,
    2,
    3 )
"""


ad_count_query_cat_bq = """
SELECT 
  sessions_date, 
  category, 
  region, 
  COUNT(*) AS ad_count 
FROM ( 
  SELECT 
    user_id, 
    sessions_date, 
    category ,
    region
  FROM ( 
    SELECT 
      device.advertising_id AS user_id, 
      event_date AS sessions_date, 
      MIN(event_date) OVER(PARTITION BY user_pseudo_id) AS first_date, 
      %category_key% AS category ,
      geo.country as region
    FROM 
      TABLE_DATE_RANGE([the-cooking-game.analytics_153765032.events_], 
                        TIMESTAMP('%start_dt_key%'),   
                        TIMESTAMP('%end_dt_key%')) 
    WHERE 
      event_name == 'ad_viewed'  
          ) 
) 
group by  
    sessions_date, 
    category ,
    region
"""


ad_count_query_cat_bq_af = """
select 
    bq.sessions_date as sessions_date,
    apps.category as category,
    bq.region as region,
    count(*) as ad_count
from
    (select 
        device.advertising_id as user_id,  
        event_date as sessions_date, 
        min(event_date) OVER(PARTITION BY user_id) as first_date,  
        geo.country as region
    from  
        TABLE_DATE_RANGE([the-cooking-game.analytics_153765032.events_],  
                         TIMESTAMP('%start_dt_key%'),   
                        TIMESTAMP('%end_dt_key%'))
    where event_name == 'ad_viewed'  ) as bq
    left join 
    (
    SELECT
      IFNULL(IDFA, Advertising_ID) as user_id,
      %category_key% as category,
      count(*)
    FROM
      TABLE_DATE_RANGE([the-cooking-game.appsflyer_IOS.in_app_events_],
        TIMESTAMP('%start_dt_key%'),   
                        TIMESTAMP('%end_dt_key%')),
      TABLE_DATE_RANGE([the-cooking-game.appsflyer_android.in_app_events_],
        TIMESTAMP('%start_dt_key%'),   
                        TIMESTAMP('%end_dt_key%'))
    group by 
        user_id,
        category
    ) as apps
    on bq.user_id = apps.user_id
group by
  sessions_date,
  category,
  region
"""


ad_count_query_bq = """
SELECT 
  sessions_date,
  region,
  COUNT(*) AS ad_count
FROM ( 
  SELECT 
    user_id,
    sessions_date,
    region
  FROM (
    SELECT
      device.advertising_id AS user_id,
      event_date AS sessions_date,
      MIN(event_date) OVER(PARTITION BY user_pseudo_id) AS first_date,
      geo.country AS region 
    FROM 
      TABLE_DATE_RANGE([the-cooking-game.analytics_153765032.events_],
                        TIMESTAMP('%start_dt_key%'),   
                        TIMESTAMP('%end_dt_key%')))
    WHERE 
      event_name == 'ad_viewed' ) 
GROUP BY 
  sessions_date,
  region
"""


revenue_count_query_cat_bq = """
select  
    sessions_date, 
    category, 
    sum(ifnull(event_value_in_usd, 0)) as revenue ,
    count(distinct(user_id)) as users_number 
from 
( 
    select 
        user_id, 
        sessions_date, 
        event_value_in_usd, 
        category 
    from 
    ( 
        select 
            device.advertising_id as user_id,  
            event_date as sessions_date, 
            min(event_date) OVER(PARTITION BY user_id) as first_date,  
            event_value_in_usd, 
            %category_key% as category  
        from  
            TABLE_DATE_RANGE([the-cooking-game.analytics_153765032.events_],  
                             TIMESTAMP('%start_dt_key%'),   
                             TIMESTAMP('%end_dt_key%'))  
    ) 
) 
group by 
    sessions_date, 
    category 
"""


revenue_count_query_cat_bq_af = """
select 
    bq.sessions_date as sessions_date,
    apps.category as category,
    sum(bq.revenue) as revenue, 
    count(distinct(bq.user_id)) as users_number 
from
    (select 
        device.advertising_id as user_id,  
        event_date as sessions_date, 
        min(event_date) OVER(PARTITION BY user_id) as first_date,  
        event_value_in_usd as revenue
    from  
        TABLE_DATE_RANGE([the-cooking-game.analytics_153765032.events_],  
                         TIMESTAMP('%start_dt_key%'),   
                             TIMESTAMP('%end_dt_key%'))  ) as bq
    left join 
    (
    SELECT
      IFNULL(IDFA, Advertising_ID) as user_id,
      %category_key% as category,
      count(*)
    FROM
      TABLE_DATE_RANGE([the-cooking-game.appsflyer_IOS.in_app_events_],
        TIMESTAMP('%start_dt_key%'),   
                             TIMESTAMP('%end_dt_key%')),
      TABLE_DATE_RANGE([the-cooking-game.appsflyer_android.in_app_events_],
        TIMESTAMP('%start_dt_key%'),   
                             TIMESTAMP('%end_dt_key%'))
    group by 
        user_id,
        category
    ) as apps
    on bq.user_id = apps.user_id
group by 
  sessions_date,
  category
"""


revenue_count_query_bq = """
select 
    sessions_date, 
    sum(ifnull(event_value_in_usd, 0)) as revenue ,
    count(distinct(user_id)) as users_number 
from 
( 
    select 
        user_id, 
        sessions_date, 
        event_value_in_usd
    from 
    ( 
        select 
            device.advertising_id as user_id,  
            event_date as sessions_date, 
            min(event_date) OVER(PARTITION BY user_id) as first_date,  
            event_value_in_usd 
        from  
            TABLE_DATE_RANGE([the-cooking-game.analytics_153765032.events_],  
                             TIMESTAMP('%start_dt_key%'),   
                             TIMESTAMP('%end_dt_key%'))  
    ) 
) 
group by 
    sessions_date
"""