from google.cloud import bigquery
from google.cloud.bigquery.job import QueryJobConfig


def get_data(query, credentials_path, path_to_save=None):

    """
    Create query 
    :param query:
    :param credentials_path:
    :param path_to_save:
    :return:
    """

    import os
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = credentials_path

    job_config = QueryJobConfig()
    job_config.use_legacy_sql = False
    bigquery_client = bigquery.Client()

    data = bigquery_client.query(query, job_config=job_config).to_dataframe()

    if path_to_save is not None:
        data.to_csv(path_to_save)
    # print(data)
    return data


def get_retention_date():

    import pandas as pd

    path = 'results-20200222-113035.csv'
    data = pd.read_csv(path)

    data.columns = ['first_date',
                    'retention_date',
                    'users_number',
                    'all_user_number',
                    'category']

    data.first_date = pd.to_datetime(data.first_date, format = '%Y%m%d')
    data['ret'] = data.users_number / data.all_user_number

    return data
