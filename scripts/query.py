start_dt_key = '%start_dt_key%'
end_dt_key = '%end_dt_key%'
category_dt_key = '%category_key%'


categories = ['traffic_source.name',
              'device.category',
              'app_info.version']

import datetime


def generate_query(base_query, day_count, category):

    """
    Replace all data in query. Day_count - start_date = now - day_count
    :param base_query:
    :param day_count:
    :param category:
    :return:
    """

    end_date = datetime.datetime.now().date().strftime('%Y%m%d')
    start_date = (datetime.datetime.now().date() - datetime.timedelta(days=day_count)).strftime('%Y%m%d')

    query = base_query.replace(start_dt_key, start_date)
    query = query.replace(end_dt_key, end_date)
    query = query.replace(category_dt_key, category)
    query = query.replace('\n', ' ')

    return query
