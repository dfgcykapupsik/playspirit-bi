from scripts.bigquery import get_data
from scripts.query import generate_query
from scripts.metrics import calculate_arpdau, classical_log_ltv
import datetime
from google.oauth2 import service_account
import pandas as pd
import logging


def init_logger():
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[
            logging.FileHandler('log/run' + str(datetime.datetime.now()) + ".log"),
            logging.StreamHandler()
        ]
    )


def category_iter(ad_query, inapp_query, ret_query, credentials_path,
                  save_path, category, ecpm_d, days_count, iter_num):

    """
    Process all queries and save arpdau, ltv, retention files for all categories
    :param ad_query:
    :param inapp_query:
    :param ret_query:
    :param credentials_path:
    :param save_path:
    :param category:
    :param ecpm_d:
    :param days_count:
    :param iter_num:
    :return:
    """

    logging.info('Start processing ' + category + ' category.')

    query_inapp = generate_query(inapp_query, days_count, category)
    query_ad = generate_query(ad_query, days_count, category)
    query_ret = generate_query(ret_query, days_count, category)

    data_inapp = get_data(query_inapp, credentials_path = credentials_path)
    data_ad = get_data(query_ad, credentials_path = credentials_path)
    data_ret = get_data(query_ret, credentials_path = credentials_path)

    # data_ret['retention'] = data_ret.users_number / data_ret.all_user_number

    arpdau = calculate_arpdau(ecpm_d, data_ad, data_inapp)
    data_ret['category_name'] = save_path
    # ltv = calculate_ltv(data_ret, arpdau)
    revenue_table = arpdau.copy()
    revenue_table['category_name'] = save_path


    credentials = service_account.Credentials.from_service_account_file(credentials_path)
    data_ret.to_csv('data/ret.csv',
                    mode = 'w' if iter_num == 0 else 'a',
                    header = iter_num == 0)
    revenue_table.to_csv('data/revenue_table.csv',
                         mode = 'w' if iter_num == 0 else 'a',
                         header = iter_num == 0)
    revenue_table = revenue_table.reset_index()
    data_ret['sessions_date'] = pd.to_datetime(data_ret['sessions_date'])
    revenue_table['sessions_date'] = pd.to_datetime(revenue_table['sessions_date'])

    # data_ret.to_gbq('BI_Results.retention_',
    #                 project_id = 'the-cooking-game',
    #                 credentials = credentials,
    #                 if_exists = 'replace' if iter_num == 0 else 'append')
    # revenue_table.columns = ['category', 'first_date', 'sessions_date',
    #                'lifetime_retention', 'users_count',
    #                'revenue_inapp', 'revenue_ad', 'category_name']
    # revenue_table.to_gbq('BI_Results.revenue_',
    #                      project_id = 'the-cooking-game',
    #                      credentials = credentials,
    #                      if_exists = 'replace' if iter_num == 0 else 'append')

    print(data_ret.head(5))
    print(revenue_table.head(5))



def get_ecpm(row, ecpm_d):
    dt = row['sessions_date']
    return ecpm_d[dt][row['os']] if dt in ecpm_d.keys() else 0.0



def ltv_preparation(revenue_table, forecast_day_count):

    os = 'IOS'
    source = 'Facebook Ads'
    country = 'United States'

    d = None
    for device in ['tablet', 'mobile']:
        ind = (revenue_table.os == os) & \
              (revenue_table.media_source == source) & \
              (revenue_table.device == device) & \
              (revenue_table.country == country)

        inapp = revenue_table[ind].groupby('sessions_date').inapp_revenue.sum().cumsum() / revenue_table[ind].groupby('sessions_date').users_number.sum()
        ad = revenue_table[ind].groupby('sessions_date').ad_revenue.sum().cumsum() / revenue_table[ind].groupby('sessions_date').users_number.sum()

        inapp_ltv_forecasting = classical_log_ltv(inapp,
                                                  len(inapp) // 5 * 4,
                                                  forecast_day_count)
        ad_ltv_forecasting = classical_log_ltv(ad,
                                               len(inapp) // 5 * 4,
                                               forecast_day_count)

        data = pd.concat([inapp_ltv_forecasting, ad_ltv_forecasting, inapp, ad], axis=1)
        data['device'] = device
        d = pd.concat([d, data])

    d = d.reset_index()
    d.columns = ['date', 'inapp_f', 'ad_f', 'inapp', 'ad', 'device']

    return d



def category_iter2(revenue_query, ret_query, credentials_path,
                  save_path, ecpm_d, days_count):

    """
    Process all queries and save arpdau, ltv, retention files for all categories
    :param revenue_query:
    :param ret_query:
    :param credentials_path:
    :param save_path:
    :param ecpm_d:
    :param days_count:
    :return:
    """

    logging.info('Start processing category.')

    query_inapp = generate_query(revenue_query, days_count, '')
    # query_ret = generate_query(ret_query, days_count, '')

    revenue_table = get_data(query_inapp, credentials_path = credentials_path)
    # revenue_table = pd.read_csv('data/revenue_table.csv')
    # del revenue_table['Unnamed: 0']
    # print(revenue_table.head(4))
    # data_ret = get_data(query_ret, credentials_path = credentials_path)

    # data_ret['retention'] = data_ret.users_number / data_ret.all_user_number

    # arpdau = calculate_arpdau(ecpm_d, data_ad, data_inapp)
    # data_ret['category_name'] = save_path
    # ltv = calculate_ltv(data_ret, arpdau)
    # revenue_table = arpdau.copy()
    # revenue_table['category_name'] = save_path

    revenue_table['sessions_date'] = pd.to_datetime(revenue_table['sessions_date'], format='%Y%m%d')
    revenue_table['ad_count'] = revenue_table['ad_count'].fillna(0)
    revenue_table['ad_revenue'] = revenue_table.apply(lambda row: row['ad_count'] / 1000 * get_ecpm(row, ecpm_d), axis=1)

    ltv = ltv_preparation(revenue_table, 180)

    credentials = service_account.Credentials.from_service_account_file(credentials_path)
    # data_ret.to_csv('data/ret.csv',
    #                 mode = 'w')
    # revenue_table.to_csv('data/revenue_table.csv',
    #                      mode = 'w')
    # revenue_table = revenue_table.reset_index()
    # data_ret['sessions_date'] = pd.to_datetime(data_ret['sessions_date'])

    # data_ret.to_gbq('BI_Results.retention_',
    #                 project_id = 'the-cooking-game',
    #                 credentials = credentials,
    #                 if_exists = 'replace')
    # print(ltv.head(5))
    # print(ltv.tail(5))
    revenue_table.columns = ['sessions_date', 'device', 'os', 'app_version', 'adset',
                             'ad', 'campaign', 'media_source', 'country',
                             'inapp_revenue', 'ad_count', 'users_number', 'ad_revenue']
    revenue_table.to_gbq('BI_Results.revenue_',
                         project_id = 'the-cooking-game',
                         credentials = credentials,
                         if_exists = 'replace')
    print(ltv.head(5))
    ltv.to_gbq('BI_Results.ltv_',
                project_id = 'the-cooking-game',
                credentials = credentials,
                if_exists = 'replace')
