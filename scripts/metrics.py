import pandas as pd


def calculate_arpdau(ecpm_dict, ad_df, inapp_df):

    """
    CALCULATE ARPDAU
    :param ecpm_dict:
    :param ad_df:
    :param inapp_df:
    :param just_ad:
    :param just_inapp:
    :return:
    """
    ad_df['revenue'] = ad_df.region.apply(lambda x: ecpm_dict.get(x, ecpm_dict['Other'])['value']) * ad_df.ad_count

    groupby_ind = ['sessions_date', 'category']

    revenue_inapp = inapp_df.groupby(groupby_ind).revenue.sum()
    revenue_ad = ad_df.groupby(groupby_ind).revenue.sum()

    user_count = inapp_df.groupby(groupby_ind).users_number.last()
    ad_arpdau = revenue_ad / inapp_df.groupby(groupby_ind).users_number.last()
    d = pd.DataFrame(user_count)
    # d['arpdau_ad'] = ad_arpdau
    d['revenue_inapp'] = revenue_inapp
    d['revenue_ad'] = revenue_ad
    d = d.reset_index()
    d.columns = ['sessions_date', 'category', 'user_count', 'revenue_inapp', 'revenue_ad']
    d = d.fillna(0)

    return d



def lifetime_retention(ret):

    """
    Calculate lifetime retention
    :param ret:
    :return:
    """

    import pandas as pd

    lifetime_retention = []
    for cat in ret.category.unique():
        ind_cat = ret[ret.category == cat].index
        for first_date in sorted(ret.loc[ind_cat].first_date.unique()):
            ind_fd = ret[ret.first_date == first_date].index

            d = pd.DataFrame(ret.loc[(ind_cat) & (ind_fd)].sort_values('sessions_date', ascending = True).set_index(
                ['category', 'first_date', 'sessions_date']).retention.cumsum())
            lifetime_retention.append(d)

    return pd.concat(lifetime_retention)



def calculate_ltv(ret, arpdau):

    """
    Calculate ltv
    :param ret:
    :param arpdau:
    :return:
    """

    lifetime_retention_ = lifetime_retention(ret)
    lifetime_retention_['arpdau_ad'] = arpdau.set_index(['category', 'first_date', 'sessions_date']).arpdau_ad
    lifetime_retention_['arpdau_inapp'] = arpdau.set_index(['category', 'first_date', 'sessions_date']).arpdau_inapp

    lifetime_retention_['ltv_inapp'] = lifetime_retention_.arpdau_inapp * lifetime_retention_.retention
    lifetime_retention_['ltv_ad'] = lifetime_retention_.arpdau_ad * lifetime_retention_.retention

    lifetime_retention_ = lifetime_retention_.fillna(0)

    return lifetime_retention_



def classical_log_ltv(real_ltv, day_count, forecast_day_count):
    # real_ltv - series

    import datetime
    import numpy as np

    start_dt = real_ltv.index[0]
    forecast_date_range = [start_dt + datetime.timedelta(days = x) for x in range(forecast_day_count)]

    # index 0 - day 1
    b = real_ltv[0]
    a_ = []
    for i in [day_count // 2, day_count]:
        a_.append((real_ltv[i] - b) / np.log(i + 1))
    a = np.mean(a_)

    ltv = []
    for i in range(1, forecast_day_count + 1):
        ltv.append(a * np.log(i) + b)

    return pd.DataFrame(ltv, index = forecast_date_range)
