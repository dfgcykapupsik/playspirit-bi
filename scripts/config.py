import yaml


def get_config(config_path):
    with open(config_path, 'r') as f:
        doc = yaml.load(f)
        return doc