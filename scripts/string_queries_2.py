revenue_q = """
WITH bq_revenue AS (SELECT 
                        device.advertising_id  as user_id, 
                        event_date AS sessions_date,  
                        device.category AS category_bq1,
                        device.operating_system AS category_bq2,
                        app_info.version AS category_bq3,
                        geo.country AS country,
                        ifnull(event_value_in_usd, 0) as revenue
                    FROM `the-cooking-game.analytics_153765032.events_*`
                    WHERE _TABLE_SUFFIX BETWEEN '%start_dt_key%' AND '%end_dt_key%'),
    bq_ad AS (SELECT 
                        device.advertising_id  as user_id, 
                        event_date AS sessions_date,  
                        device.category AS category_bq1,
                        device.operating_system AS category_bq2,
                        app_info.version AS category_bq3,
                        geo.country AS country
                    FROM `the-cooking-game.analytics_153765032.events_*`
                    WHERE _TABLE_SUFFIX BETWEEN '%start_dt_key%' AND '%end_dt_key%' and event_name = 'ad_viewed'),
     union_af_os AS (SELECT 
                        IDFA, Advertising_ID, Adset, Ad, Campaign, Media_Source
                     FROM 
                        `the-cooking-game.appsflyer_IOS.in_app_events_*`
                     WHERE _TABLE_SUFFIX BETWEEN '%start_dt_key%' AND '%end_dt_key%'
                     UNION ALL
                     SELECT 
                         IDFA, Advertising_ID, Adset, Ad, Campaign, Media_Source
                     FROM 
                        `the-cooking-game.appsflyer_android.in_app_events_*`
                     WHERE _TABLE_SUFFIX BETWEEN '%start_dt_key%' AND '%end_dt_key%'),
     cat_af AS (SELECT
                      IFNULL(IDFA, Advertising_ID) as user_id,
                      Adset as category_af1,
                      Ad as category_af2,
                      Campaign as category_af3,
                      Media_Source as category_af4
                   FROM union_af_os
                   )
     
SELECT 
    t1.sessions_date as sessions_date,
    t1.cat1 as device,
    t1.cat2 as os,
    t1.cat3 as app_version,
    t1.cat4 as adset,
    t1.cat5 as ad,
    t1.cat6 as campaign,
    t1.cat7 as media_source, 
    t1.country as country,
    t1.inapp_revenue as inapp_revenue,
    t2.ad_count as ad_count,
    t1.users_number as users_number
FROM
(
    (SELECT cat_1.sessions_date as sessions_date, 
          cat_1.category_bq1 as cat1, 
          cat_1.category_bq2 as cat2, 
          cat_1.category_bq3 as cat3, 
          cat_2.category_af1 as cat4, 
          cat_2.category_af2 as cat5, 
          cat_2.category_af3 as cat6, 
          cat_2.category_af4 as cat7, 
          cat_1.country as country,
          sum(cat_1.revenue) as inapp_revenue,
          count(distinct(cat_1.user_id)) as users_number
    FROM bq_revenue as cat_1
    LEFT JOIN cat_af as cat_2 ON cat_1.user_id = cat_2.user_id
    GROUP BY
          1,2,3,4,5,6,7,8, 9) as t1
    LEFT JOIN
    (SELECT cat_1.sessions_date as sessions_date, 
          cat_1.category_bq1 as cat1, 
          cat_1.category_bq2 as cat2, 
          cat_1.category_bq3 as cat3, 
          cat_2.category_af1 as cat4, 
          cat_2.category_af2 as cat5, 
          cat_2.category_af3 as cat6, 
          cat_2.category_af4 as cat7, 
          cat_1.country as country,
          count(*) as ad_count
    FROM bq_ad as cat_1
    LEFT JOIN cat_af as cat_2 ON cat_1.user_id = cat_2.user_id
    GROUP BY
          1,2,3,4,5,6,7,8, 9) as t2
    ON t1.sessions_date = t2.sessions_date and t1.cat1 = t2.cat1 and t1.cat2 = t2.cat2 and t1.cat3 = t2.cat3 and t1.cat4 = t2.cat4 and t1.cat5 = t2.cat5 and t1.cat6 = t2.cat6 and t1.cat7 = t2.cat7 and t1.country = t2.country
)
"""