from scripts.string_queries_2 import revenue_q
import pandas as pd
from scripts.config import get_config
from scripts.category import category_iter2
from scripts.sheets import get_sheet
from scripts.category import init_logger
import os


if __name__ == "__main__":

    config = get_config(os.path.join(os.path.dirname(__file__), 'config/config.yaml'))
    ios_ecpm = get_sheet('17qtyO_M3Ajmg7NfXnYC0COV56Nar2Hieo33npZMzaPo',
                         'A2:B',
                         os.path.join(os.path.dirname(__file__), config['sheets_key_path']),
                         ['Date', 'IOS'],
                         os.path.join(os.path.dirname(__file__), config['sheets_token_path']))
    android_ecpm = get_sheet('1A-B9Kr8fyzIIRI7GFiyHeUy76x0zl3igk7_FYbJDHjs',
                             'A2:B',
                             os.path.join(os.path.dirname(__file__), config['sheets_key_path']),
                             ['Date', 'ANDROID'],
                             os.path.join(os.path.dirname(__file__), config['sheets_token_path']))

    ios_ecpm['Date'] = pd.to_datetime(ios_ecpm['Date'], format = '%d/%m/%Y')
    android_ecpm['Date'] = pd.to_datetime(android_ecpm['Date'], format = '%d/%m/%Y')
    ios_ecpm = ios_ecpm.set_index('Date')
    android_ecpm = android_ecpm.set_index('Date')
    ios_ecpm['IOS'] = ios_ecpm['IOS'].astype(float)
    android_ecpm['ANDROID'] = android_ecpm['ANDROID'].astype(float)
    ecpm = pd.concat([ios_ecpm, android_ecpm], axis=1)
    ecpm_d = ecpm.to_dict('index')

    # init_logger()

    category_iter2(revenue_q,
                   None,
                   os.path.join(os.path.dirname(__file__), config['key_path']),
                   '',
                   ecpm_d,
                   config['days_count'])
