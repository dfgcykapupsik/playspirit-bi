from scripts.string_queries import *
import pandas as pd
from scripts.config import get_config
from scripts.category import category_iter
from scripts.sheets import get_sheet
from scripts.category import init_logger


if __name__ == "__main__":

    ecpm = pd.read_csv('eCPM.csv')
    ecpm_d = ecpm.set_index('region').to_dict('index')
    config = get_config('./config/config.yaml')
    # get_sheet('https://docs.google.com/spreadsheets/d/1vW4GVWqx45aL08KB2OSR9Fo9BC4ebiDjHCVQCfsGxCc/edit?usp=sharing',
    #           config['key_path'])
    # 1/0

    bq_cat = ['traffic_source.name',
              'device.category',
              'device.operating_system',
              'app_info.version']
    bq_path = ['traffic',
               'device',
               'os',
               'app_version']

    init_logger()

    iter_num = 0
    for cat, path in zip(bq_cat, bq_path):
        category_iter(ad_count_query_cat_bq,
                      revenue_count_query_cat_bq,
                      retention_query_bq,
                      config['key_path'],
                      path,
                      cat,
                      ecpm_d,
                      config['days_count'],
                      iter_num)
        iter_num += 1

    af_cat = ['Adset',
              'Ad',
              'Campaign',
              'Media_Source']
    af_path = ['adset',
               'ad',
               'campaign',
               'media_source']

    for cat, path in zip(af_cat, af_path):
        category_iter(ad_count_query_cat_bq_af,
                      revenue_count_query_cat_bq_af,
                      retention_query_bq_af,
                      config['key_path'],
                      path,
                      cat,
                      ecpm_d,
                      config['days_count'],
                      iter_num)
        iter_num += 1
