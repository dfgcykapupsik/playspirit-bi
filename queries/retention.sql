-- select
--     user_pseudo_id,
--     sessions_date,
--     first_date,
--     DATEDIFF(first_date, sessions_date) as retention_date
-- from(
--     select
--         user_pseudo_id,
--         event_date as sessions_date,
--         min(event_date) OVER(PARTITION BY user_pseudo_id) as first_date
--     from
--         [the-cooking-game.analytics_153765032.events_20200218]
-- --         where
-- --             activity_type = any_type
-- --             and cdate >= '2020-01-12'
--     )
-- group by
--     1,2,3

            
select
     first_date,
     retention_date,
     users_number,
     max (users_number) OVER(PARTITION BY first_date)
from(
    select
        first_date,
        retention_date,
        sum(case when user_pseudo_id is not null then 1 else 0 end) as users_number
    from(
        select
            user_pseudo_id,
            sessions_date,
            first_date,
            DATEDIFF(first_date, sessions_date) as retention_date
        from(
            select
                user_pseudo_id,
                event_date as sessions_date,
                min(event_date) OVER(PARTITION BY user_pseudo_id) as first_date
            from
                TABLE_DATE_RANGE([the-cooking-game.analytics_153765032.events_], 
                                 TIMESTAMP('2020-02-06'), 
                                 TIMESTAMP('2020-02-18'))
            )
        group by
            1,2,3,4
        )
    group by
        1,2
)

-- retention by category
select
     first_date,
     retention_date,
     users_number,
     max (users_number) OVER(PARTITION BY first_date) as all_user_number,
     category
from(
    select
        first_date,
        retention_date,
        category,
        sum(case when user_pseudo_id is not null then 1 else 0 end) as users_number
    from(
        select
            user_pseudo_id,
            sessions_date,
            first_date,
            DATEDIFF(sessions_date, first_date) as retention_date,
            category
        from(
            select
                user_pseudo_id,
                event_date as sessions_date,
                min(event_date) OVER(PARTITION BY user_pseudo_id) as first_date,
                device.mobile_model_name as category
            from
                TABLE_DATE_RANGE([the-cooking-game.analytics_153765032.events_],
                                 TIMESTAMP('2020-02-16'),
                                 TIMESTAMP('2020-02-18'))
            )
        group by
            1,2,3,4,5
        )
    group by
        1,2,3
)