select
    Event_Name
from
    TABLE_DATE_RANGE([the-cooking-game.appsflyer_IOS.in_app_events_], 
                     TIMESTAMP('2019-11-16'), 
                     TIMESTAMP('2020-02-18'))
where
    Event_Name == 'af_revenue'
-- group by 
--     Country_Code,
--     user_id
    
select Event_Name
from
    TABLE_DATE_RANGE([the-cooking-game.appsflyer_IOS.in_app_events_], 
                     TIMESTAMP('2019-11-16'), 
                     TIMESTAMP('2020-02-18'))
group by
Event_Name

    
    
    
select
event_date,
event_timestamp,
    event_name,
    event_params.key,
    event_params.value.double_value,
    event_params.value.float_value,
    event_params.value.string_value,
    event_params.value.int_value
from
    TABLE_DATE_RANGE([the-cooking-game.analytics_153765032.events_], 
                     TIMESTAMP('2019-11-16'), 
                     TIMESTAMP('2020-02-18'))
where
    event_name == 'in_app_purchase' and
    (event_params.key == 'price' or event_params.key == 'currency')
    
    
    
    
select event_name
from
    TABLE_DATE_RANGE([the-cooking-game.analytics_153765032.events_], 
                     TIMESTAMP('2019-11-16'), 
                     TIMESTAMP('2020-02-18'))
group by
event_name




select
    Country_Code,
    IFNULL(IDFA, IDFV) as user_id,
    COUNT(*)
from
    TABLE_DATE_RANGE([the-cooking-game.appsflyer_IOS.in_app_events_], 
                     TIMESTAMP('2020-02-18'), 
                     TIMESTAMP('2020-02-18'))
where
    Event_Name == 'ad_viewed'
group by 
    Country_Code,
    user_id