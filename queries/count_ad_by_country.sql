-- Запросы для appsflyer

select
    Event_Time as date,  
    Country_Code,
    IFNULL(IDFA, IDFV) as user_id,
    COUNT(*)
from
    TABLE_DATE_RANGE([the-cooking-game.appsflyer_IOS.in_app_events_], 
                     TIMESTAMP('2020-02-18'), 
                     TIMESTAMP('2020-02-18'))
where
    Event_Name == 'ad_viewed'
group by 
    date ,    
    Country_Code,
    user_id

--

select
    Event_Time as date,  
    Country_Code,
    IFNULL(IDFA, IDFV) as user_id,
    COUNT(*),
    Media_Source, Device_Type, OS_Version, Adset, Ad, App_Version
from
    TABLE_DATE_RANGE([the-cooking-game.appsflyer_IOS.in_app_events_], 
                     TIMESTAMP('2020-02-18'), 
                     TIMESTAMP('2020-02-18'))
where
    Event_Name == 'ad_viewed'
group by 
    date ,      
    Country_Code,
    user_id,
    Media_Source, Device_Type, OS_Version, Adset, Ad, App_Version



-- Запросы для firebase

select
    user_id,
    day_date,
    category,
    sum(ifnull(event_value_in_usd, 0)) as revenue
from
(
    select
        user_id,
        sessions_date,
        first_date,
        DATEDIFF(sessions_date, first_date) as day_date,
        event_value_in_usd,
        category
    from
    (
        select
            IFNULL( device.vendor_id, device.advertising_id ) as user_id,
            event_date as sessions_date,
            min(event_date) OVER(PARTITION BY user_pseudo_id) as first_date,
            event_value_in_usd,
            device.mobile_model_name as category
        from
            TABLE_DATE_RANGE([the-cooking-game.analytics_153765032.events_],
                             TIMESTAMP('2020-02-16'),
                             TIMESTAMP('2020-02-18'))
    )
)
group by
    user_id,
    day_date,
    category

---

select
    user_id,
    day_date,
    category,
    count(*) as ad_count
from
(
    select
        user_id,
        sessions_date,
        first_date,
        DATEDIFF(sessions_date, first_date) as day_date,
        category
    from
    (
        select
            IFNULL( device.vendor_id, device.advertising_id ) as user_id,
            event_date as sessions_date,
            min(event_date) OVER(PARTITION BY user_pseudo_id) as first_date,
            device.mobile_model_name as category
        from
            TABLE_DATE_RANGE([the-cooking-game.analytics_153765032.events_],
                             TIMESTAMP('2020-02-16'),
                             TIMESTAMP('2020-02-18'))
        where
            event_name == 'ad_viewed'
    )
)
group by
    user_id,
    day_date,
    category

bq
traffic_source.name, device.category, device.operating_system, app_info.version

af
Adset, Ad, Campaign, Media_Source