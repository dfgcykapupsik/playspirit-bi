select
     first_date
    ,retention_date
    ,users_number
    ,cast(users_number as real) / max (users_number) OVER(PARTITION BY first_date) as ret
    ,max (users_number) OVER(PARTITION BY first_date)
from(
    select
        first_dat
        ,retention_date
        ,sum(case when user_id is not null then 1 else 0 end) as users_number
    from(
        select
            user_id
            ,sessions_date
            ,first_date
            ,date_diff('day', first_date, sessions_date) as retention_date
        from(
            select
                user_id
                ,date("date") as sessions_date
                ,min(date("date")) OVER(PARTITION BY user_id) as first_date
            from
                any_table
            where
                activity_type = any_type
                and cdate >= '2020-01-12'
            )
        group by
            1,2,3
        )
    group by
        1,2
)